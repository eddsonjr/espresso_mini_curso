package com.edsonjr.espresso_curso.PageObjects.Login

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.edsonjr.espresso_curso.MainActivity
import com.edsonjr.espresso_curso.PageObjects.Login.robot.RobotLogin
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class TestLogin {

    //instanciando a robot login
    private var robot = RobotLogin()

    @get:Rule
    var mActivityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun testeLoginModeloPage(){
        robot.validarTituloNaTela()
        robot.escreverEmail()
        robot.fecharTeclado()
        robot.escreverSenha()
        robot.fecharTeclado()
        robot.clicarEmLogar()
        robot.validarUsuarioLogado()
    }
}