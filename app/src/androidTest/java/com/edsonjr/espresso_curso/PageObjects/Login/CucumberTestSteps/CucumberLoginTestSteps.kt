package com.edsonjr.espresso_curso.PageObjects.Login.CucumberTestSteps

import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edsonjr.espresso_curso.MainActivity
import com.edsonjr.espresso_curso.PageObjects.Login.robot.RobotLogin
import cucumber.api.java.es.Dado

class CucumberLoginTestSteps {

    private val robot = RobotLogin()

    private var mActivityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Dado("Eu entro na tela de login")
    fun startLoginScreen(){
        ActivityScenario.launch(MainActivity::class.java)
    }

}