package com.edsonjr.espresso_curso.PageObjects.Login.robot

import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.edsonjr.espresso_curso.PageObjects.Login.constants.ConstantsLogin
import com.edsonjr.espresso_curso.R


//esta classe trabalha com todas as acoes utilizadas durante os cenarios de testes
class RobotLogin {

    fun validarTituloNaTela(){
        onView(withText(ConstantsLogin.TITULO_APLICATIVO))
            .check(matches(isDisplayed()))
    }

    fun escreverEmail() {
        onView(withId(CAMPO_EMAIL))
            .perform(typeText(ConstantsLogin.EMAIL_SUCESSO))
    }

    fun escreverSenha(){
        onView(withId(CAMPO_SENHA)).perform(typeText(ConstantsLogin.SENHA_TESTE))
    }


    fun fecharTeclado(){
        closeSoftKeyboard()
    }


    fun clicarEmLogar(){
        onView(withId(BOTAO_LOGAR)).perform(ViewActions.click())
    }


    fun validarUsuarioLogado() {
        onView(withText(ConstantsLogin.TEXTO_LOGADO)).check(matches(isDisplayed()))
    }


    companion object {
        private val CAMPO_EMAIL = R.id.login_editText
        private val CAMPO_SENHA = R.id.password_editText
        private val BOTAO_LOGAR = R.id.button_1
    }

}