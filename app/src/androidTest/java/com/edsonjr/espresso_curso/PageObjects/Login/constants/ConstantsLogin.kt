package com.edsonjr.espresso_curso.PageObjects.Login.constants


//constantes usadas nos testes
object ConstantsLogin {
    val TITULO_APLICATIVO = "Aplicativo de testes para Espresso"
    val EMAIL_SUCESSO = "email@email.com"
    val SENHA_TESTE = "123456"
    val TEXTO_LOGADO = "Usuário logado com sucesso!"
}