package com.edsonjr.espresso_curso

import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith



/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */


@RunWith(AndroidJUnit4::class)
class TesteFormatoBase {

    private val textoCabecalho = "Aplicativo de testes para Espresso"
    private val emailTeste = "exemplo@exemplo.com"
    private val passwordTest = "1234"

    //antes de qualquer teste, configura a activity que vamos usar nos testes
    @get:Rule
    var mActivityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)



    //validando se o texto da tela de login esta visivel na tela
    @Test
    fun verificarTextoCabecalho() {
        onView(withText(textoCabecalho)).check(matches(isDisplayed()))
    }


    //este teste serve para inserior os dados nos campos de texto e validar os testes
    @Test
    fun inserirDadosNosCampos() {
        onView(withId(R.id.login_editText)).perform(typeText(emailTeste))
        closeSoftKeyboard() //fecha o teclado
        onView(withId(R.id.password_editText)).perform(typeText(passwordTest))
        closeSoftKeyboard()

        //realiza o click sobre o botao
        onView(withId(R.id.button_1)).perform(click())
    }

}