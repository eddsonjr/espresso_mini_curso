#language: pt

  Funcionalidade: Login do usuario

    @login-validando
    Cenario: Valida a tela de login
      Dado Eu entro na tela de login
      Quando seleciono o campo de email
      E digito o email <email>
      E seleciono o campo de senha
      E digito a senha <senha>
      E clico no botao de Login
      Entao carrego a tela home

        | email             | senha  |
        | exemplo@email.com | 123456 |